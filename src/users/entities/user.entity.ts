export class User {
    id: number;
    login: string;
    password: string;
    role: ('admin' | 'user')[];
    gender: 'male' | 'female'
    age: number
}
