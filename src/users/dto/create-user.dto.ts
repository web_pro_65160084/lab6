export class CreateUserDto {
    login: string;
    password: string;
    role: ('admin' | 'user')[];
    gender: 'male' | 'female'
    age: number
}
