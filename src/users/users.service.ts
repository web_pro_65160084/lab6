import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';

@Injectable()
export class UsersService {
  lastId: number = 1;
  user: User[] = [{
    id: 1,
    login: 'admin',
    password: 'Pass@1234',
    role: ['admin'],
    gender: 'male',
    age: 50
  }]
  create(createUserDto: CreateUserDto) {
    this.lastId++
    const newUser = {...createUserDto, id: this.lastId}
    this.user.push(newUser)
    return newUser;
  }

  findAll() {
    return this.user;
  }

  findOne(id: number) {
    const index = this.user.findIndex((user)=> user.id===id)
    if(index<0){
      throw new NotFoundException()
    }
    return this.user[index];
  }

  update(id: number, updateUserDto: UpdateUserDto) {
    const index = this.user.findIndex((user)=> user.id===id)
    if(index<0){
      throw new NotFoundException()
    }
    this.user[index] = {...this.user[index], ...updateUserDto}
    return this.user[index];
  }

  remove(id: number) {
    const index = this.user.findIndex((user)=> user.id===id)
    if(index<0){
      throw new NotFoundException()
    }
    const delUser = this.user[index]
    this.user.splice(index, 1)
    return delUser;
  }
}
